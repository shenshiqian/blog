class Comment < ActiveRecord::Base
  belongs_to :post
  validates :post, presence: true
  validates :post_id, :body, presence: true 
end
